package tddmicroexercises.telemetrysystem;

import java.net.ConnectException;

public class TelemetryDiagnosticControls {

	private String diagnosticChannelConnectionString = "*111#";
	private final Client client;
	private String diagnosticInfo;

	public TelemetryDiagnosticControls(Client client, String diagnosticChannelConnectionString) {
		this.client = client;
		this.diagnosticChannelConnectionString = diagnosticChannelConnectionString;
	}

	public TelemetryDiagnosticControls() {
		client = new TelemetryClient();
	}

	public String getDiagnosticInfo() {
		return diagnosticInfo;
	}

	public void checkTransmission() throws ConnectException {
		client.disconnect();
		tryToConnect();
		if (client.getOnlineStatus()) {
			client.send(TelemetryClient.DIAGNOSTIC_MESSAGE);
			diagnosticInfo = client.receive();
		} else {
			throw new ConnectException("Unable to connect.");
		}
	}

	private void tryToConnect() {
		int retryLeft = 3;
		while (!client.getOnlineStatus() && retryLeft > 0) {
			client.connect(diagnosticChannelConnectionString);
			retryLeft -= 1;
		}
	}
}
