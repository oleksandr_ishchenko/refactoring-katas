package tddmicroexercises.telemetrysystem;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.net.ConnectException;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;

class TelemetryDiagnosticControlsTest {

    TelemetryDiagnosticControls diagnosticControls;
    TelemetryDiagnosticControls diagnosticControlsWithParameters;

    @BeforeEach
    void initialization() {
        diagnosticControls = new TelemetryDiagnosticControls();
        diagnosticControlsWithParameters = new TelemetryDiagnosticControls(new TelemetryClient(), "*100#");
    }

    @Test
    void CheckTransmission_should_send_a_diagnostic_message_and_receive_a_status_message_response() throws ConnectException {
        diagnosticControls.checkTransmission();
        String forTest = "LAST TX rate................ 100 MBPS\r\n" + "HIGHEST TX rate............. 100 MBPS\r\n"
                + "LAST RX rate................ 100 MBPS\r\n" + "HIGHEST RX rate............. 100 MBPS\r\n"
                + "BIT RATE.................... 100000000\r\n" + "WORD LEN.................... 16\r\n"
                + "WORD/FRAME.................. 511\r\n" + "BITS/FRAME.................. 8192\r\n"
                + "MODULATION TYPE............. PCM/FM\r\n" + "TX Digital Los.............. 0.75\r\n"
                + "RX Digital Los.............. 0.10\r\n" + "BEP Test.................... -5\r\n"
                + "Local Rtrn Count............ 00\r\n" + "Remote Rtrn Count........... 00";
        assertEquals(forTest, diagnosticControls.getDiagnosticInfo());
    }

    @Test
    void CheckTransmissionTestWithInjectedDiagnosticChannelConnectionString() throws ConnectException {
        diagnosticControlsWithParameters.checkTransmission();
        String forTest = "LAST TX rate................ 100 MBPS\r\n" + "HIGHEST TX rate............. 100 MBPS\r\n"
                + "LAST RX rate................ 100 MBPS\r\n" + "HIGHEST RX rate............. 100 MBPS\r\n"
                + "BIT RATE.................... 100000000\r\n" + "WORD LEN.................... 16\r\n"
                + "WORD/FRAME.................. 511\r\n" + "BITS/FRAME.................. 8192\r\n"
                + "MODULATION TYPE............. PCM/FM\r\n" + "TX Digital Los.............. 0.75\r\n"
                + "RX Digital Los.............. 0.10\r\n" + "BEP Test.................... -5\r\n"
                + "Local Rtrn Count............ 00\r\n" + "Remote Rtrn Count........... 00";
        assertEquals(forTest, diagnosticControlsWithParameters.getDiagnosticInfo());
    }

    @Test
    void CheckTransmissionTestWithInjectedDiagnosticChannelConnectionStringIfConnectionStringIsNull() {
        diagnosticControlsWithParameters = new TelemetryDiagnosticControls(new TelemetryClient(), null);
        assertThrows(IllegalArgumentException.class, () ->  diagnosticControlsWithParameters.checkTransmission());
    }
}
