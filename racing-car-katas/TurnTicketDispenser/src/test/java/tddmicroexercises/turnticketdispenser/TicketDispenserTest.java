package tddmicroexercises.turnticketdispenser;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

class TicketDispenserTest {

    TicketDispenser dispenser;

    @BeforeEach
    void initialization() {
        dispenser = new TicketDispenser();
    }

    @Test
    @DisplayName("method must return TurnTicket object with new ticket number inside")
    void getTurnTicket() {
        dispenser.getTurnTicket();
        new TicketDispenser().getTurnTicket();
        new TicketDispenser().getTurnTicket();
        dispenser.getTurnTicket();
        assertEquals(4, dispenser.getTurnTicket().getTurnNumber());
    }
}