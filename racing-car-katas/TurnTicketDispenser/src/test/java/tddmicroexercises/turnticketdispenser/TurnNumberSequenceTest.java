package tddmicroexercises.turnticketdispenser;

import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

class TurnNumberSequenceTest {

    @Test
    @DisplayName("method must increment static variable turnNumber and return it")
    void getNextTurnNumber() {
        TurnNumberSequence.getNextTurnNumber();
        assertEquals(1, TurnNumberSequence.getNextTurnNumber());
    }
}