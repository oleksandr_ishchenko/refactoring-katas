package tddmicroexercises.turnticketdispenser;

public class TurnNumberSequence {

	private static int turnNumber;

	private TurnNumberSequence() {
	}

	public static int getNextTurnNumber() {
		return turnNumber++;
	}
}
