package tddmicroexercises.turnticketdispenser;

public class TicketDispenser {

	public TurnTicket getTurnTicket() {
		return new TurnTicket(TurnNumberSequence.getNextTurnNumber());
	}
}
