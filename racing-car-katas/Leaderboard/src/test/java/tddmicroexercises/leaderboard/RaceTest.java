package tddmicroexercises.leaderboard;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

class RaceTest {

    @Test
    void isShouldCalculateDriverPoints() {
        // setup

        // act

        // verify
        assertEquals(25, TestData.race1.getPoints(TestData.driver1));
        assertEquals(18, TestData.race1.getPoints(TestData.driver2));
        assertEquals(15, TestData.race1.getPoints(TestData.driver3));
    }


    @Test
    void position() {
        assertEquals(0, TestData.race1.position(TestData.driver1));
        assertEquals(1, TestData.race1.position(TestData.driver2));
        assertEquals(2, TestData.race1.position(TestData.driver3));
    }
}
