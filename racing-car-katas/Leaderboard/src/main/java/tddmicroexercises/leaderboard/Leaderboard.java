package tddmicroexercises.leaderboard;

import java.util.*;
import java.util.stream.Collector;
import java.util.stream.Collectors;

public class Leaderboard {

    private final List<Race> races;

    public Leaderboard(Race... races) {
        this.races = Arrays.asList(races);
    }

    public Map<String, Integer> driverResults() {
        return this.races.stream()
                .map(this::getDriverPoints)
                .map(Map::entrySet)
                .flatMap(Collection::stream)
                .collect(Collectors.groupingBy(Map.Entry::getKey,
                        Collectors.summingInt(Map.Entry::getValue)));
    }

    private Map<String, Integer> getDriverPoints(Race race) {
        return race.getResults().stream()
                .collect(Collector.of(
                        HashMap<String, Integer>::new,
                        (map, driver) -> map.compute(
                                driver.getName(),
                                (k, v) -> v != null ? v + race.getPoints(driver) : race.getPoints(driver)),
                        (left, right) -> left
                ));
    }

    public List<String> driverRankings() {
        return driverResults().keySet().stream()
                .sorted()
                .collect(Collectors.toList());
    }
}
