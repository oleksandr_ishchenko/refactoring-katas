package tddmicroexercises.leaderboard;

import java.util.*;
import java.util.stream.Collectors;

public class Race {

    private final String name;
    private final List<Driver> results;
    private final Map<Driver, String> driverNames;

    public Race(String name, Driver... drivers) {
        this.name = name;
        this.results = Arrays.asList(drivers);
        this.driverNames = getDriverNames();
    }

    private Map<Driver, String> getDriverNames() {
        if (results != null) {
            return results.stream()
                    .collect(Collectors.toMap(driver -> driver, Driver::getName, (a, b) -> b));
        }

        return Collections.emptyMap();
    }

    public int position(Driver driver) {
        return this.results.indexOf(driver);
    }

    public int getPoints(Driver driver) {
        return Points.getPOINTS()[position(driver)];
    }

    public List<Driver> getResults() {
        return results;
    }

    public String getDriverName(Driver driver) {
        return this.driverNames.get(driver);
    }

    @Override
    public String toString() {
        return name;
    }
}
