package tddmicroexercises.leaderboard;

public class Points {

    private static final Integer[] AVAILABLE_POINTS = new Integer[]{25, 18, 15};

    private Points() {
    }

    public static Integer[] getPOINTS() {
        return AVAILABLE_POINTS;
    }
}