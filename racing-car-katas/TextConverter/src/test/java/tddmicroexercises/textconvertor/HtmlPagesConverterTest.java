package tddmicroexercises.textconvertor;

import org.junit.jupiter.api.Test;

import java.io.IOException;

import static org.junit.jupiter.api.Assertions.assertEquals;

class HtmlPagesConverterTest {

    @Test
    void getHtmlPageTest() throws IOException {
        HtmlPagesConverter converter = new HtmlPagesConverter("./src/test/resources/test.txt");
        assertEquals("<br />&quot;Hello World!!!&quot;<br />", converter.getHtmlPage(0));
    }
}
