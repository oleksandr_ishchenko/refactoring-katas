package tddmicroexercises.textconvertor;

import org.junit.jupiter.api.Test;

import java.io.IOException;

import static org.junit.jupiter.api.Assertions.assertEquals;

class HtmlTextConverterTest {

    @Test
    void convertToHtmlTest() throws IOException {
        HtmlTextConverter textConverter= new HtmlTextConverter("./src/test/resources/test.txt");
        assertEquals("&quot;Hello World!!!&quot;<br />PAGE_BREAK<br />&quot;Hello World!!!&quot;<br />", textConverter.convertToHtml());
    }
}
