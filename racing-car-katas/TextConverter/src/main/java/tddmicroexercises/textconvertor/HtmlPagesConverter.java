package tddmicroexercises.textconvertor;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collector;

public class HtmlPagesConverter {

    private final String filename;
    private final List<Integer> breaks;

    public HtmlPagesConverter(String filename) throws IOException {
        this.filename = filename;
        this.breaks = read();
    }

    private List<Integer> read() throws IOException {
        try (BufferedReader reader = new BufferedReader(new FileReader(filename))) {
            return reader.lines()
                    .collect(Collector.of(
                            ArrayList<Integer>::new,
                            (list, string) -> list.add(string.length() + 1),
                            (left, right) -> left
                    ));
        }
    }

    public String getHtmlPage(int page) throws IOException {
        try (BufferedReader reader = new BufferedReader(new FileReader(this.filename))) {
            reader.skip(breaks.get(page));
            return reader.lines()
                    .filter(s -> !s.contains("PAGE_BREAK"))
                    .collect(Collector.of(StringBuilder::new,
                            ((stringBuilder, s) -> stringBuilder.append(StringEscapeUtils.escapeHtml(s)).append("<br />")),
                            (left, right) -> left)).toString();
        }
    }
}
