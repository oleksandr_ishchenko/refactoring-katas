package tddmicroexercises.textconvertor;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.util.stream.Collector;

public class HtmlTextConverter {

    private final String fullFilenameWithPath;

    public HtmlTextConverter(String fullFilenameWithPath) {
        this.fullFilenameWithPath = fullFilenameWithPath;
    }

    public String convertToHtml() throws IOException {
        StringBuilder html;
        try (BufferedReader reader = new BufferedReader(new FileReader(fullFilenameWithPath))) {
            html = reader.lines()
                    .collect(Collector.of(StringBuilder::new,
                            ((stringBuilder, s) -> stringBuilder.append(StringEscapeUtils.escapeHtml(s)).append("<br />")),
                            (left, right) -> left));
        }

        return html.toString();
    }
}
