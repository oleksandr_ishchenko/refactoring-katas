package tddmicroexercises.tirepressuremonitoringsystem;

public class PressureThresholdSafetyLevel {

    private double lowPressureThreshold = 17;
    private double highPressureThreshold = 21;

    public PressureThresholdSafetyLevel(double lowPressureThreshold, double highPressureThreshold) {
        this.lowPressureThreshold = lowPressureThreshold;
        this.highPressureThreshold = highPressureThreshold;
    }

    public PressureThresholdSafetyLevel() {
    }

    public double getLowPressureThreshold() {
        return lowPressureThreshold;
    }

    public double getHighPressureThreshold() {
        return highPressureThreshold;
    }
}
