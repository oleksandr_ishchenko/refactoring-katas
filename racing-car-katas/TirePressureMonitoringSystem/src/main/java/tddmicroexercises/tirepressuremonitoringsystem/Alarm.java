package tddmicroexercises.tirepressuremonitoringsystem;

public class Alarm {

	private final PressureThresholdSafetyLevel safetyLevel;
	private final Sensor sensor;
	private boolean alarmOn = false;

	public Alarm(Sensor sensor, PressureThresholdSafetyLevel safetyLevel) {
		this.sensor = sensor;
		this.safetyLevel = safetyLevel;
	}

	public Alarm() {
		this (new Sensor(), new PressureThresholdSafetyLevel());
	}

	public void check() {
		if (isSafetyRange(getPressureValue())) {
			alarmOn = true;
		}
	}

	public boolean isAlarmOn() {
		return alarmOn;
	}

	private boolean isSafetyRange(double psiPressureValue) {
		return psiPressureValue < safetyLevel.getLowPressureThreshold() || safetyLevel.getHighPressureThreshold() < psiPressureValue;
	}

	private double getPressureValue() {
		return sensor.popNextPressurePsiValue();
	}
}
