package tddmicroexercises.tirepressuremonitoringsystem;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;

import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertTrue;

class TestAlarm {

    Alarm mockedAlarm;
    Alarm alarm;
    Sensor sensorMock;

    @BeforeEach
    void initialization() {
    	alarm = new Alarm();
        sensorMock = Mockito.mock(Sensor.class);
		mockedAlarm = new Alarm(sensorMock, new PressureThresholdSafetyLevel(17, 21));
    }

	@Test
	@DisplayName("method must return false if method check is never used")
	void checkTest() {
		assertFalse(alarm.isAlarmOn());
	}

    @Test
    @DisplayName("method must return false if pressureValue in range of 17 - 21")
    void checkTestWhenSensorIsMockedAndPressureInSafetyRange() {
        Mockito.when(sensorMock.popNextPressurePsiValue()).thenReturn(19.0);
        mockedAlarm.check();
        assertFalse(mockedAlarm.isAlarmOn());
    }

	@Test
	@DisplayName("method must return true if pressureValue out of range of 17 - 21")
	void checkTestWhenSensorIsMockedAndPressureOutOfSafetyRange() {
		Mockito.when(sensorMock.popNextPressurePsiValue()).thenReturn(15.0);
		mockedAlarm.check();
		assertTrue(mockedAlarm.isAlarmOn());
	}
}
