import java.util.Objects;

public class TennisGame1 implements TennisGame {

    private int firstPlayerScore = 0;
    private int secondPlayerScore = 0;
    private String firstPlayerName;
    private String secondPlayerName;

    public TennisGame1(String firstPlayerName, String secondPlayerName) {
        this.firstPlayerName = firstPlayerName;
        this.secondPlayerName = secondPlayerName;
    }

    public void wonPoint(String playerName) {
        if (Objects.equals(this.firstPlayerName, playerName)) {
            firstPlayerScore++;
        }
        if (Objects.equals(this.secondPlayerName, playerName)) {
            secondPlayerScore++;
        }
    }

    public String getScore() {
        if (firstPlayerScore == secondPlayerScore) {
            return drawMessage(firstPlayerScore);
        } else if (firstPlayerScore >= 4 || secondPlayerScore >= 4) {
            return getFavorite(getMinResult(firstPlayerScore, secondPlayerScore));
        } else {

            return getCurrentScore(firstPlayerScore, secondPlayerScore);
        }
    }

    private String getFavorite(int minResult) {
        if (minResult == 1) {
            return "Advantage " + firstPlayerName;
        } else if (minResult == -1) {
            return "Advantage " + secondPlayerName;
        } else if (minResult > 1) {
            return "Win for " + firstPlayerName;
        } else {

            return "Win for " + secondPlayerName;
        }
    }

    private int getMinResult(int mScore1, int mScore2) {
        return mScore1 - mScore2;
    }

    private String getCurrentScore(int mScore1, int mScore2) {
        return getPlayerScore(mScore1) + "-" + getPlayerScore(mScore2);
    }

    private String getPlayerScore(int playerScore) {
        switch (playerScore) {
            case 0:
                return "Love";
            case 1:
                return "Fifteen";
            case 2:
                return "Thirty";
            default:
                return "Forty";
        }
    }

    private String drawMessage(int playerScore) {
        switch (playerScore) {
            case 0:
                return "Love-All";
            case 1:
                return "Fifteen-All";
            case 2:
                return "Thirty-All";
            default:
                return "Deuce";
        }
    }
}
