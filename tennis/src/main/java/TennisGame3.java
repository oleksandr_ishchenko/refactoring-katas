import java.util.Objects;

public class TennisGame3 implements TennisGame {

    private int firstPlayerScore;
    private int secondPlayerScore;
    private String firstPlayerName;
    private String secondPlayerName;

    public TennisGame3(String firstPlayerName, String secondPlayerName) {
        this.firstPlayerName = firstPlayerName;
        this.secondPlayerName = secondPlayerName;
    }

    public void wonPoint(String playerName) {
        if (Objects.equals(this.firstPlayerName, playerName)) {
            firstPlayerScore++;
        }
        if (Objects.equals(this.secondPlayerName, playerName)) {
            secondPlayerScore++;
        }
    }

    public String getScore() {
        if (secondPlayerScore < 4 && firstPlayerScore < 4 && (secondPlayerScore + firstPlayerScore != 6)) {
            return getCurrentScore();
        } else {
            return secondPlayerScore == firstPlayerScore ? "Deuce" : getFavorite();
        }
    }

    private String getFavorite() {
        String playerName = secondPlayerScore < firstPlayerScore ? firstPlayerName : secondPlayerName;
        return (getScoreDifferent() == 1) ? "Advantage " + playerName : "Win for " + playerName;
    }

    private int getScoreDifferent() {
        return (secondPlayerScore - firstPlayerScore) * (secondPlayerScore - firstPlayerScore);
    }

    private String getCurrentScore() {
        String[] availableScore = new String[]{"Love", "Fifteen", "Thirty", "Forty"};
        return (secondPlayerScore == firstPlayerScore) ? availableScore[secondPlayerScore] + "-All" : availableScore[firstPlayerScore] + "-" + availableScore[secondPlayerScore];
    }
}
