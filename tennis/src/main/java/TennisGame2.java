import java.util.Objects;

public class TennisGame2 implements TennisGame {

    private int firstPlayerScore = 0;
    private int secondPlayerScore = 0;
    private String firstPlayerName;
    private String secondPlayerName;

    public TennisGame2(String firstPlayerName, String secondPlayerName) {
        this.firstPlayerName = firstPlayerName;
        this.secondPlayerName = secondPlayerName;
    }

    public void wonPoint(String player) {
        if (Objects.equals(this.firstPlayerName, player)) {
            firstPlayerScore++;
        }
        if (Objects.equals(this.secondPlayerName, player)) {
            secondPlayerScore++;
        }
    }

    public String getScore() {
        String score = (firstPlayerScore == secondPlayerScore) ? getDrawScore() : getCurrentScore();
        score = getFavorite(score);

        return score;
    }

    private String getDrawScore() {
        return firstPlayerScore < 3 ? getPlayerScore(firstPlayerScore) + "-All" : "Deuce";
    }

    private String getCurrentScore() {
        return getPlayerScore(firstPlayerScore) + "-" + getPlayerScore(secondPlayerScore);
    }

    private String getFavorite(String score) {
        if (firstPlayerScore > secondPlayerScore && secondPlayerScore > 2) {
            score = "Advantage " + firstPlayerName;
        }
        if (secondPlayerScore > firstPlayerScore && firstPlayerScore > 2) {
            score = "Advantage " + secondPlayerName;
        }
        if (firstPlayerScore > 3 && (firstPlayerScore - secondPlayerScore) > 1) {
            score = "Win for " + firstPlayerName;
        }
        if (secondPlayerScore > 3 && (secondPlayerScore - firstPlayerScore) > 1) {
            score = "Win for " + secondPlayerName;
        }

        return score;
    }

    private String getPlayerScore(int playerPoint) {
        String playerScore = "";
        if (playerPoint == 0) {
            playerScore = "Love";
        }
        if (playerPoint == 1) {
            playerScore = "Fifteen";
        }
        if (playerPoint == 2) {
            playerScore = "Thirty";
        }
        if (playerPoint == 3) {
            playerScore = "Forty";
        }

        return playerScore;
    }
}