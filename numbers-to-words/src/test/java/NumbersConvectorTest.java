import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertAll;
import static org.junit.jupiter.api.Assertions.assertEquals;

class NumbersConvectorTest {

    NumbersConvector convector;

    @BeforeEach
    @DisplayName("")
    public void objectInitialization() {
        convector = new NumbersConvector();
    }

    @Test
    @DisplayName("method must take the number from 0 to 9 as an argument and return it as a string")
    void numberConvectorOneDigit() {
        assertAll(
                () -> assertEquals("zero", convector.convertNumberToStringRepresentations(0)),
                () -> assertEquals("one", convector.convertNumberToStringRepresentations(1)),
                () -> assertEquals("two", convector.convertNumberToStringRepresentations(2)),
                () -> assertEquals("three", convector.convertNumberToStringRepresentations(3)),
                () -> assertEquals("four", convector.convertNumberToStringRepresentations(4)),
                () -> assertEquals("five", convector.convertNumberToStringRepresentations(5)),
                () -> assertEquals("six", convector.convertNumberToStringRepresentations(6)),
                () -> assertEquals("seven", convector.convertNumberToStringRepresentations(7)),
                () -> assertEquals("eight", convector.convertNumberToStringRepresentations(8)),
                () -> assertEquals("nine", convector.convertNumberToStringRepresentations(9))
        );
    }

    @Test
    @DisplayName("method must take the number from 10 to 19 as an argument and return it as a string")
    void numberConvectorTwoDigitsFromTenToNineteen() {
        assertAll(
                () -> assertEquals("ten", convector.convertNumberToStringRepresentations(10)),
                () -> assertEquals("eleven", convector.convertNumberToStringRepresentations(11)),
                () -> assertEquals("twelve", convector.convertNumberToStringRepresentations(12)),
                () -> assertEquals("thirteen", convector.convertNumberToStringRepresentations(13)),
                () -> assertEquals("fourteen", convector.convertNumberToStringRepresentations(14)),
                () -> assertEquals("fifteen", convector.convertNumberToStringRepresentations(15)),
                () -> assertEquals("sixteen", convector.convertNumberToStringRepresentations(16)),
                () -> assertEquals("seventeen", convector.convertNumberToStringRepresentations(17)),
                () -> assertEquals("eighteen", convector.convertNumberToStringRepresentations(18)),
                () -> assertEquals("nineteen", convector.convertNumberToStringRepresentations(19))
        );
    }

    @Test
    @DisplayName("method must take only number from 20 to 99 as an argument and return it as a string")
    void numberConvectorTwoDigits() {
        assertAll(
                () -> assertEquals("twenty", convector.convertNumberToStringRepresentations(20)),
                () -> assertEquals("thirty-two", convector.convertNumberToStringRepresentations(32)),
                () -> assertEquals("forty-four", convector.convertNumberToStringRepresentations(44)),
                () -> assertEquals("fifty-one", convector.convertNumberToStringRepresentations(51)),
                () -> assertEquals("sixty-seven", convector.convertNumberToStringRepresentations(67)),
                () -> assertEquals("seventy-nine", convector.convertNumberToStringRepresentations(79)),
                () -> assertEquals("eighty", convector.convertNumberToStringRepresentations(80)),
                () -> assertEquals("ninety-nine", convector.convertNumberToStringRepresentations(99))
        );
    }

    @Test
    @DisplayName("method must take only number from 100 to 999 as an argument and return it as a string")
    void numberConvectorThreeDigits() {
        assertAll(
                () -> assertEquals("two hundred one", convector.convertNumberToStringRepresentations(201)),
                () -> assertEquals("three hundred twenty", convector.convertNumberToStringRepresentations(320)),
                () -> assertEquals("four hundred forty", convector.convertNumberToStringRepresentations(440)),
                () -> assertEquals("five hundred ten", convector.convertNumberToStringRepresentations(510)),
                () -> assertEquals("six hundred seventy-five", convector.convertNumberToStringRepresentations(675)),
                () -> assertEquals("seven hundred ninety-three", convector.convertNumberToStringRepresentations(793)),
                () -> assertEquals("eight hundred four", convector.convertNumberToStringRepresentations(804)),
                () -> assertEquals("nine hundred ninety-nine", convector.convertNumberToStringRepresentations(999))
        );
    }

    @Test
    @DisplayName("method must take only number from 1000 to 9999 as an argument and return it as a string")
    void numberConvectorFourDigits() {
        assertAll(
                () -> assertEquals("two thousand seventeen", convector.convertNumberToStringRepresentations(2017)),
                () -> assertEquals("thirty-two hundred six", convector.convertNumberToStringRepresentations(3206)),
                () -> assertEquals("forty-four hundred", convector.convertNumberToStringRepresentations(4400)),
                () -> assertEquals("fifty-one hundred eight", convector.convertNumberToStringRepresentations(5108)),
                () -> assertEquals("sixty-seven hundred fifty-four", convector.convertNumberToStringRepresentations(6754)),
                () -> assertEquals("seventy-nine hundred thirty-nine", convector.convertNumberToStringRepresentations(7939)),
                () -> assertEquals("eight thousand forty-one", convector.convertNumberToStringRepresentations(8041)),
                () -> assertEquals("nine thousand", convector.convertNumberToStringRepresentations(9000))
        );
    }
}

