public class NumbersConvector {

    public String convertNumberToStringRepresentations(int numberToConvert) {
        if (numberToConvert < 20) {
            return convertorFromZeroToNineteen(numberToConvert);
        } else if (numberToConvert < 100) {
            return convertorFromTwentyToNinetyNine(numberToConvert);
        } else if (numberToConvert < 1000) {
            return convertorFromOneHundredToNineHundredNinetyNine(numberToConvert);
        } else if (numberToConvert < 10000){
            return convertorFromOneThousandToNinetyNineHundredNinetyNine(numberToConvert);
        }

        else return "undefine";
    }

    private String convertorFromZeroToNineteen(int numberToConvert) {
        String[] numbersFromZeroToNineteen = new String[]{"zero", "one", "two", "three", "four", "five",
                "six", "seven", "eight", "nine", "ten", "eleven", "twelve", "thirteen",
                "fourteen", "fifteen", "sixteen", "seventeen", "eighteen", "nineteen"};

        return numbersFromZeroToNineteen[numberToConvert];
    }

    private String convertorFromTwentyToNinetyNine(int numberToConvert) {
        String[] presentationOfDozens = new String[]{"twenty", "thirty", "forty", "fifty", "sixty", "seventy", "eighty", "ninety"};

        return numberToConvert % 10 == 0
                ? presentationOfDozens[numberToConvert / 10 - 2]
                : presentationOfDozens[numberToConvert / 10 - 2] + "-" + convertorFromZeroToNineteen(numberToConvert % 10);
    }

    private String convertorFromOneHundredToNineHundredNinetyNine(int numberToConvert) {
        return convertorFromZeroToNineteen(numberToConvert / 100) + " hundred "
                + (numberToConvert % 100 != 0
                ? convertNumberToStringRepresentations(numberToConvert % 100)
                : "");
    }

    private String convertorFromOneThousandToNinetyNineHundredNinetyNine(int numberToConvert) {
        String convertedString = numberToConvert / 100 % 10 != 0
                ? convertNumberToStringRepresentations(numberToConvert / 100)
                : convertNumberToStringRepresentations(numberToConvert / 1000);

        return convertedString
                + messageIfSecondDigitInFourDigitsNumberZeroOrNot(numberToConvert)
                + roundNumberRepresentations(numberToConvert)
                + messageIfSecondDigitInTwoDigitsNumberZeroOrNot(numberToConvert % 100);
    }

    private String roundNumberRepresentations(int checkNumber) {
        return checkNumber % 10 == 0 && checkNumber % 100 == 0 ? "" : " ";
    }

    private String messageIfSecondDigitInFourDigitsNumberZeroOrNot(int numberToConvert) {
        return (numberToConvert / 100) % 10 == 0
                ? " thousand"
                : " hundred";
    }

    private String messageIfSecondDigitInTwoDigitsNumberZeroOrNot(int numberToConvert) {
        return numberToConvert % 10 == 0
                ? ""
                : convertNumberToStringRepresentations(numberToConvert % 100);
    }
}




