public class Main {

    public static void main(String[] args) {

        NumbersConvector convector = new NumbersConvector();

        for (int i = 0; i < 10000; i++) {
            System.out.println(convector.convertNumberToStringRepresentations(i));
        }
    }
}
