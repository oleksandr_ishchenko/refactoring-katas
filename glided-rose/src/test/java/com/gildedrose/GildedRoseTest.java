package com.gildedrose;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertAll;
import static org.junit.jupiter.api.Assertions.assertEquals;

class GildedRoseTest {

    GildedRose gildedRose;
    Item[] items;

    @BeforeEach
    public void initialization() {

    }

    @Test
    @DisplayName("Some item quality deteriorating in proportion to age, and if sellIn is < 0 deteriorates twice as fast")
    void updateQuality1() {
        items = new Item[]{
                new Item("Some item", 5, 45)
        };
        gildedRose = new GildedRose(items);
        for (int i = 0; i < 10; i++) {
            gildedRose.updateQuality();
        }
        assertAll(
                () -> assertEquals(30, items[0].quality),
                () -> assertEquals(-5, items[0].sellIn)
        );
    }

    @Test
    @DisplayName("Aged Brie quality increases in proportion to age, and cannot be more than 50")
    void updateQuality() {
        items = new Item[]{
                new Item("Aged Brie", 10, 45)
        };
        gildedRose = new GildedRose(items);
        for (int i = 0; i < 6; i++) {
            gildedRose.updateQuality();
        }
        assertAll(
                () -> assertEquals(50, items[0].quality),
                () -> assertEquals(4, items[0].sellIn)
        );
    }

    @Test
    @DisplayName("The quality of a product can never be denied")
    void updateQualityDeniedTest() {
        items = new Item[]{
                new Item("Some Item", 0, 2)
        };
        gildedRose = new GildedRose(items);
        for (int i = 0; i < 6; i++) {
            gildedRose.updateQuality();
        }
        assertAll(
                () -> assertEquals(0, items[0].quality),
                () -> assertEquals(-6, items[0].sellIn)
        );
    }

    @Test
    @DisplayName("Backstage passes quality increases in proportion to age, and cannot be more than 50")
    void updateBackstagePassesQuality() {
        items = new Item[]{
                new Item("Backstage passes to a TAFKAL80ETC concert", 13, 48)
        };
        gildedRose = new GildedRose(items);
        for (int i = 0; i < 4; i++) {
            gildedRose.updateQuality();
        }
        assertAll(
                () -> assertEquals(50, items[0].quality),
                () -> assertEquals(9, items[0].sellIn)
        );
    }

    @Test
    @DisplayName("if the number of days is less than 11 the quality of Backstage passes will grow by 2 per day")
    void updateBackstageQualityIfSellInLowerThan11() {
        items = new Item[]{
                new Item("Backstage passes to a TAFKAL80ETC concert", 10, 28)
        };
        gildedRose = new GildedRose(items);
        for (int i = 0; i < 2; i++) {
            gildedRose.updateQuality();
        }
        assertAll(
                () -> assertEquals(32, items[0].quality),
                () -> assertEquals(8, items[0].sellIn)
        );
    }

    @Test
    @DisplayName("if the number of days is less than 6 the quality of Backstage passes will grow by 3 per day")
    void updateBackstageQualityIfSellInLowerThan5() {
        items = new Item[]{
                new Item("Backstage passes to a TAFKAL80ETC concert", 5, 28)
        };
        gildedRose = new GildedRose(items);
        for (int i = 0; i < 2; i++) {
            gildedRose.updateQuality();
        }
        assertAll(
                () -> assertEquals(34, items[0].quality),
                () -> assertEquals(3, items[0].sellIn)
        );
    }

    @Test
    @DisplayName("if the number of days is 0, the quality of Backstage passes is also set to 0")
    void updateBackstageQualityIfSellInISZero() {
        items = new Item[]{
                new Item("Backstage passes to a TAFKAL80ETC concert", 2, 28)
        };
        gildedRose = new GildedRose(items);
        for (int i = 0; i < 3; i++) {
            gildedRose.updateQuality();
        }
        assertAll(
                () -> assertEquals(0, items[0].quality),
                () -> assertEquals(-1, items[0].sellIn)
        );
    }

    @Test
    @DisplayName("Sulfuras cannot reduce quality")
    void updateSulfurasQuality() {
        items = new Item[]{
                new Item("Sulfuras, Hand of Ragnaros", 10, 80)
        };
        gildedRose = new GildedRose(items);
        for (int i = 0; i < 10; i++) {
            gildedRose.updateQuality();
        }
        assertAll(
                () -> assertEquals(80, items[0].quality),
                () -> assertEquals(10, items[0].sellIn)
        );
    }

    @Test
    @DisplayName("Conjured products lose quality twice as fast as regular products")
    void updateConjuredQuality() {
        items = new Item[]{
                new Item("Conjured", 10, 80)
        };
        gildedRose = new GildedRose(items);
        for (int i = 0; i < 10; i++) {
            gildedRose.updateQuality();
        }
        assertAll(
                () -> assertEquals(60, items[0].quality),
                () -> assertEquals(0, items[0].sellIn)
        );
    }
}