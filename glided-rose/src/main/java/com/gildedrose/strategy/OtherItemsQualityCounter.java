package com.gildedrose.strategy;

import com.gildedrose.Item;

public class OtherItemsQualityCounter implements QualityCounter {

    @Override
    public void qualityCount(Item item) {
        item.quality = updateQualityOtherItems(item.sellIn, item.quality);
    }

    private int updateQualityOtherItems(int sellIn, int quality) {
        if (quality > 0) {
            return sellIn > 0 ? quality - 1 : quality - 2;
        }

        return quality;
    }
}
