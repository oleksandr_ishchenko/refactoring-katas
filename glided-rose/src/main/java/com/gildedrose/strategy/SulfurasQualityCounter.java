package com.gildedrose.strategy;

import com.gildedrose.Item;

public class SulfurasQualityCounter implements QualityCounter {

    @Override
    public void qualityCount(Item item) {
            //it is a magical object. cannot be changed.
    }
}
