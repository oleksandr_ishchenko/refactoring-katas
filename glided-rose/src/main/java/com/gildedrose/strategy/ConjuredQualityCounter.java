package com.gildedrose.strategy;

import com.gildedrose.Item;

public class ConjuredQualityCounter implements QualityCounter {

    @Override
    public void qualityCount(Item item) {
            item.quality = updateQualityConjuredItems(item.sellIn, item.quality);
    }

    private int updateQualityConjuredItems(int sellIn, int quality) {
        if (quality > 0) {
            return sellIn > 0 ? quality - 2 : quality - 4;
        }

        return quality;
    }
}
