package com.gildedrose.strategy;

import com.gildedrose.Item;

public interface QualityCounter {

     void qualityCount(Item item);
}
