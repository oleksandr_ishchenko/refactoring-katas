package com.gildedrose.strategy;

import com.gildedrose.Item;

public class BackstagePassesQualityCounter implements QualityCounter {

    @Override
    public void qualityCount(Item item) {
            item.quality = updateQualityBackstagePasses(item.sellIn, item.quality);
    }

    private int updateQualityBackstagePasses(int sellIn, int quality) {
        if (quality < 50 && sellIn != 0) {
            if (sellIn < 6) {
                return quality + 3;
            } else if (sellIn < 11) {
                return quality + 2;
            } else {
                return quality + 1;
            }
        }

        return sellIn != 0 ? 50 : 0;
    }
}
