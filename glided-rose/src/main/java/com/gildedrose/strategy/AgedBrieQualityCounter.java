package com.gildedrose.strategy;

import com.gildedrose.Item;

public class AgedBrieQualityCounter implements QualityCounter {

    @Override
    public void qualityCount(Item item) {
            item.quality = updateQualityAgedBrie(item.quality);
    }

    private int updateQualityAgedBrie(int quality) {
        if (quality < 50) {
            return quality + 1;
        }

        return 50;
    }
}
