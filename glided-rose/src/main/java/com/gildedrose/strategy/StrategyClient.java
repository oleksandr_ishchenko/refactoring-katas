package com.gildedrose.strategy;

import com.gildedrose.Item;

public class StrategyClient {

    QualityCounter qualityCounter;

    public void setQualityCounter(QualityCounter qualityCounter) {
        this.qualityCounter = qualityCounter;
    }

    public void executeQualityCount(Item item){
        qualityCounter.qualityCount(item);
    }
}
