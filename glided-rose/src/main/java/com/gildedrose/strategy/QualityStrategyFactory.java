package com.gildedrose.strategy;

import com.gildedrose.Item;

public class QualityStrategyFactory {

    public QualityCounter getItemStrategyObject(Item item) {

        if (item.name.contains("Aged Brie")) {
            return new AgedBrieQualityCounter();

        } else if (item.name.contains("Backstage passes")) {
            return new BackstagePassesQualityCounter();

        } else if (item.name.contains("Sulfuras")) {
            return new  SulfurasQualityCounter();

        } else if (item.name.contains("Conjured")) {
            return new ConjuredQualityCounter();

        }
        return new OtherItemsQualityCounter();
    }
}
