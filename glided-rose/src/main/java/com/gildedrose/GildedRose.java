package com.gildedrose;

import com.gildedrose.strategy.QualityStrategyFactory;
import com.gildedrose.strategy.StrategyClient;

class GildedRose {

    private Item[] items;
    private StrategyClient strategyClient = new StrategyClient();
    private QualityStrategyFactory qualityStrategyFactory = new QualityStrategyFactory();

    public GildedRose(Item[] items) {
        this.items = items;
    }

    public void updateQuality() {
        for (Item value : items) {
            strategyClient.setQualityCounter(qualityStrategyFactory.getItemStrategyObject(value));
            strategyClient.executeQualityCount(value);
            updateSellIn(value);
        }
    }

    private void updateSellIn(Item item) {
        if (!item.name.equals("Sulfuras, Hand of Ragnaros")) {
            item.sellIn -= 1;
        }
    }


}